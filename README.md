# Sudoku Solver GPU

Uses the power of C++ and CUDA to solve these pesky sudoku puzzles in less than 10 milliseconds per puzzle!

## Requirements:

- CUDA - capable device with compute capability 3.0+
- Installed CUDA: [CUDA Installation Guide](https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.htmlhttps://)

## Launch:

1. Navigate to the solution folder, and type the following:
   `nvcc main.cu`
2. A couple of new files will be generated (a.exe, a.lib, a.exp). Type exe name into the console:
   `a.exe`
3. Wait for the process to finish. NOTE: Process executes twice, first execution is used to load kernels into memory therefore it will take longer than any subsequent run.

In the main.cu file, you can select a puzzle difficulty from the following:

V_EASY,
EASY,
MEDIUM,
HARD,
EXTREME,
MEMORY_TEST,
UNSOLVABLE

Keep in mind memory test can crash for machines with bad GPUs.

## Solution Description

Pseudo - algorithm description of the solution:

1. Take an unsolved puzzle and input it into GPU memory
2. For every cell, launch a thread that checks what values might lie inside
3. For each board, launch a thread that finds a cell with the lowest count of possible insertions. If the count in any cell is 0 (excluding the filled cells), the board is removed.
4. Generate X new boards from every board by inserting every possible value onto the cell mentioned above
5. Repeat steps 2 - 4 until a board is full/no solution is found.
6. Print full board, or alternatively show that no solution exists.

## Results

For the hardest difficulty (EXTREME) with kernels pre-heated and 1000 input puzzles:

CPU: 11 000 ms

![](demos/cpu.png)

GPU: 9 000 ms

![](demos/gpu.png)

Speedup of 22% over CPU
